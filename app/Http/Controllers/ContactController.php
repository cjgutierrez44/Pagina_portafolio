<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Contact ;

class ContactController extends Controller
{
    //
    public function index()
	{
		
		try{
           $Contactos = Contact::all()
           ->sortByDesc('created_at');
       
        }

        catch(\Exception $e)
        {
            dd($e);
            Session::flash('message','Error'.$e->getmessage());
        }   
           

//dd($todos);
        return \View::make('messagescontact',compact('Contactos'));
	}

	public function view($id)
	{
		

		try{
       $Contactos = Contact::find($id);

			$Contactos->estado_id=2;
			$Contactos->save();

        }

        catch(\Exception $e)
        {
            dd($e);
            Session::flash('message','Error'.$e->getmessage());
        }   
           

//dd($todos);
        return \View::make('messagecontact',compact('Contactos'));
    }
	
	

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create(array $data)
	{
 	
 		  return Contact::create([
            'name' => $data['name'],
            'email' => $data['email'],
       		'subject' => $data['subject'],
       		'message' => $data['message'],
       		'estado_id' => $data['estado_id'],
        ]);

 		  		return redirect('/');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		try
		{
			$contact= new Contact;
			$contact->create($request->all());
		}
		catch(\Exception $e)
        {
 			dd($e);
            Session::flash('message','Error'.$e->getmessage());     
        }  	
		return redirect('/');
	}
}
