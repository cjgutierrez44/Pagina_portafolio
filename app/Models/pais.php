<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class pais extends Model
{
    //
     protected $table = 'paises';
     protected $fillable = ['codigo', 'nombre'];
    protected $guarded = ['id'];

}
