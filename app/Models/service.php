<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class service extends Model
{
    //
	  protected $table = 'services';
     protected $fillable = ['image', 'name','description','estado_id'];
    protected $guarded = ['id'];
}
