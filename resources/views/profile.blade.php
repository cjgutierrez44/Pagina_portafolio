@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        
 <div class="col-md-11 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Perfil</div>

                <div class="panel-body">
                <div class="col-md-3">
                <center>
                    <img src="profile_pictures/{{$Usuario->img}}"  class="img-thumbnail">
                    <br>
                    <br>
                    <a href="editarperfil" class="btn btn-perro" >Editar Perfil</a>
                </center>
                </div>
                <div class="col-md-8">

                <p><strong>Nombre: </strong> {{$Usuario->name}}</p>
                <p><strong>Apellido: </strong> {{$Usuario->last_name}}</p>
                <p><strong>Correo: </strong> {{$Usuario->email}}</p>
                <p><strong>Pais: </strong> {{$Usuario->pais->nombre}}</p>
                <p><strong>Rol: </strong> {{$Usuario->rol->nombre}}</p>
            

            </div>
                  
                </div>
            </div>
        </div>
    </div>
</div>
@endsection




