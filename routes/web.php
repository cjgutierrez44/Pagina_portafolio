<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

	Route::resource('registrar','Auth\RegisterController');


	
Route::get('/home', 'HomeController@index');

Route::post('logout',['as'=>'logout','uses'=>"Auth\LoginController@logout"]);
Route::get('/logout', function(){
	Auth::logout();
	return redirect('/');
});
Route::post('contact/store', ['as'=>'contact/store', 'uses'=>'ContactController@store']);

Route::get('message', 'ContactController@index');
Route::get('message/ver/{id}', ['as' => 'message/ver', 'uses'=>'ContactController@view']);
Route::get('message/{id}', ['as' => 'message', 'uses'=>'ContactController@view']);
Route::get('services', 'ServiceController@index');
Route::get('profile', 'ProfileController@index');
Route::get('editarperfil', 'ProfileController@edit');
Route::post('service/update', 'ServiceController@update');
Route::get('projects', 'projectController@index');
Route::get('service/add', 'ServiceController@nuevo');
Route::get('project/add', 'projectController@nuevo');
Route::post('service/store', ['as'=>'service/store', 'uses'=>'ServiceController@store']);
Route::post('project/store', ['as'=>'project/store', 'uses'=>'projectController@store']);
Route::post('guardarperfil', ['as'=>'guardarperfil', 'uses'=>'ProfileController@update']);
Route::post('project/update', 'projectController@update');
Route::get('service/{id}', ['as'=>'service', 'uses'=>'ServiceController@edit']);
Route::get('project/{id}', ['as'=>'project', 'uses'=>'projectController@edit']);
Route::get('cargaEventos{id?}','CalendarController@index');
Route::post('guardaEventos', array('as' => 'guardaEventos','uses' => 'CalendarController@create'));
Route::post('actualizaEventos','CalendarController@update');
Route::post('eliminaEvento','CalendarController@delete');
Route::post('guardarImagenes','HomeController@imagen');
Route::get('imagenes','imagenesController@index');
Auth::routes();

Route::get('/home', 'HomeController@index');
